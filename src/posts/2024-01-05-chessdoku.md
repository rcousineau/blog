# Chess-Doku

January 5, 2024

A friend got me a book called _The Chess Lover's Puzzle Book_. There's a
different (sometimes loosely) chess-related puzzle that you can do each day,
with a total of 365 puzzles. I started 4 days ago and today I'm working on
Puzzle #5. I was very excited that today's puzzle is not only chess-related, but
it's a Sudoku!

> Fill in the grid so that each row, column and 2 x 6 box contains only one each
> of the pieces shown below:
>
> * ♟︎ (black pawn)
> * ♜ (black rook)
> * ♞ (black knight)
> * ♙ (white pawn)
> * ♖ (white rook)
> * ♘ (white knight)

```
| 　 | ♘ | 　 | ♖ | 　 | 　 |
------------------------------
| 　 | ♙ | ♞ | 　 | ♜ | 　 |
------------------------------
| 　 | 　 | 　 | 　 | ♖ | 　 |
------------------------------
| ♘ | 　 | 　 | 　 | 　 | ♙ |
------------------------------
| 　 | 　 | 　 | 　 | 　 | ♟︎ |
------------------------------
| 　 | ♞ | 　 | ♙ | 　 | ♖ |
```

I started working on solving it, and I ended up here:

```
| 　 | ♘ | 　 | ♖ | ♙ | ♞ |
------------------------------
| ♖ | ♙ | ♞ | ♟︎ | ♜ | ♘ |
------------------------------
| ♞ | ♟︎ | ♙ | ♘ | ♖ | ♜ |
------------------------------
| ♘ | ♜ | ♖ | ♞ | ♟︎ | ♙ |
------------------------------
| ♙ | ♖ | ♘ | ♜ | ♞ | ♟︎ |
------------------------------
| 　 | ♞ | 　 | ♙ | ♘ | ♖ |
```

The problem is there are two solutions. The back of the book says this is the
solution:

```
| ♟︎ | ♘ | ♜ | ♖ | ♙ | ♞ |
------------------------------
| ♖ | ♙ | ♞ | ♟︎ | ♜ | ♘ |
------------------------------
| ♞ | ♟︎ | ♙ | ♘ | ♖ | ♜ |
------------------------------
| ♘ | ♜ | ♖ | ♞ | ♟︎ | ♙ |
------------------------------
| ♙ | ♖ | ♘ | ♜ | ♞ | ♟︎ |
------------------------------
| ♜ | ♞ | ♟︎ | ♙ | ♘ | ♖ |
```

But this solution works too:

```
| ♜ | ♘ | ♟︎ | ♖ | ♙ | ♞ |
------------------------------
| ♖ | ♙ | ♞ | ♟︎ | ♜ | ♘ |
------------------------------
| ♞ | ♟︎ | ♙ | ♘ | ♖ | ♜ |
------------------------------
| ♘ | ♜ | ♖ | ♞ | ♟︎ | ♙ |
------------------------------
| ♙ | ♖ | ♘ | ♜ | ♞ | ♟︎ |
------------------------------
| ♟︎ | ♞ | ♜ | ♙ | ♘ | ♖ |
```

Is it a good puzzle? Well... it got me thinking!

Fun fact: it's basically impossible to predict how wide these chess piece
unicode characters will be rendered, and it seems to be equally impossible to
render ascii or unicode spaces/dashes in a way that matches the width of the
pieces.
