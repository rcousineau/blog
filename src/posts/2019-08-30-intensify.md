# 🤩 *Rust Hype Intensifies* 🤩

August 30, 2019

I had the marvelous fortune of being able to attend RustConf 2019 a week ago,
and it was a pretty incredible experience. I got to shake hands with
[Steve Klabnik][steveklabnik] and [Sean Griffin][sgrif], which made my inner
Rust fanboy giggle uncontrollably. But the conference also just had a very cool
atmosphere. I've heard it said so many times that the Rust community is
welcoming, but that feeling got solidified in seeing the diversity of not just
attendees but also the speakers. The talks were pretty great. I can't wait for
them to hit Youtube so I can re-watch them, and so I can see the other half of
the talks from the conference.

Steve Klabnik is co-author of [The Rust Programming Language][thebook] (a.k.a.
"the book"), which is an absolute pleasure to read. There are really good
explanations and examples throughout. I'm well overdue for my third full
read-through of it. It was awesome to meet him in person and thank him for
the work he put into the book.

Sean Griffin has hosted the great podcasts [The Bike Shed][bikeshed] and
[The Yak Shave][yakshave] and appeared on a few episodes of
[New Rustacean][rustacean]. He's also the creator of [diesel][diesel]. I love
listening to him on podcasts because he constantly thinks out loud about
different programming challenges and discusses them with other folks on the
air.

Between going to a few [PDX Rust][pdxrust] meetups recently and now getting
this chance to attend RustConf, my excitement about Rust just keeps growing and
growing. For about a month, I've been working on a fun project to implement a
Rust introductory tutorial game called [Rust Warrior][rustwarrior]. It's a
spiritual successor to [Ruby Warrior][rubywarrior], which I played many years
ago while I was in college. It's been a fun project, and I've learned quite a
bit along the way.

One thing I've learned is that Rust is a **joy** to work with on a real
project. I intentionally started the project dead simple, despite having a
perfect view of the finished product. One reason for this was that I
wanted to leave myself room for taking it in a different direction than its
predecessor. But also, each time I wanted to add a new feature it required
refactoring, sometimes in non-trivial ways. Each time, I was confidently able
to use the compiler to steer me towards successful completion.

This project allowed me to scratch an itch I've had for a while. This is a
weird and obscure itch, but I've always wanted to generate one of those gifs
of a program demo in a terminal. I was able to accomplish this relatively
easily with [Terminalizer][terminalizer]. Check out the project
[readme][rustwarrior].

While working on this project, I became acquainted with [crates.io][crates.io]
and [docs.rs][docs.rs]. The default behavior of [cargo][cargo] is to publish
your crate (Rust project) to [crates.io][crates.io] when you do
`cargo publish`. In addition to your crate being available there, your
documentation is also automatically made available on [docs.rs][docs.rs].

My *heightened* degree of acquaintance with [docs.rs][docs.rs] came from me
noticing that, after publishing a release to [crates.io][crates.io] and
checking in regularly... 13 hours later my documentation was still not updated.
The [docs.rs][docs.rs] team is usually fairly responsive in discord, and I was
kindly informed that there is a [build queue][queue] which can sometimes get
blocked by crates whose docs take a very long time to build. I have since seen
that queue reach a size of over 200 crates, though at this exact moment the
queue is empty. I was also told that one aspect of the slowness is that all of
the files are uploaded to S3 sequentially. There was some talk about
parallelizing the upload, which I offered to implement. At the time of writing
this, my [pull request][pr] is still open, but it's nearly approved and merged!
This is an exciting accomplishment because I've been wanting an opportunity to
work with async Rust.

One of my favorite talks from RustConf was [Ferris Explores Rustc][rustc],
partly because it was so entertaining (again, I can't wait for this and the
other talks to all hit Youtube!); but also because it amplified my desire
to contribute to the Rust language and compiler.

For the last several weeks, I have been trying to learn everything I can about
macros because I want to do a presentation at an upcoming [PDX Rust][pdxrust]
meetup. My notes and slides are [here][macros]. This feels like a nice warm up
for jumping in to work on the compiler, since procedural macros (formerly known
as compiler extensions) give you a glimpse at some internals of the Rust
language. So my plan is to wrap up this presentation and set sail for the
`rustc`!

***THANK YOU*** to everyone at RustConf and everyone in this awesome community.

**<center>May the Rust be With You</center>**

---

P.S.

Totally unrelated to Rust, I recently built my first ever Firefox extension!
It is quite simple: it lists all of your open tabs (across all Firefox windows)
in a sidebar panel. You can click an item in the list to switch to that tab. I
use it constantly because I always have so many tabs open that their titles are
truncated down to just a few letters and an ellipsis, so I can't tell which is
which. If this is a struggle for you as well, check out
[Tab List Sidebar][tablist].

Some other rad extensions I use:

* [Dark Reader][darkreader] - dark mode on any website
* [New Tab Context][newtab] - open an ***adjacent*** new tab (I really missed this from Chrome)
* [uBlock Origin][ublock]

[steveklabnik]: https://twitter.com/steveklabnik
[sgrif]: https://twitter.com/sgrif
[thebook]: https://doc.rust-lang.org/book/
[bikeshed]: https://bikeshed.fm/
[yakshave]: http://yakshave.fm/
[rustacean]: https://newrustacean.com/
[diesel]: https://github.com/diesel-rs/diesel
[pdxrust]: https://www.meetup.com/PDXRust/
[rustwarrior]: https://github.com/miller-time/rust-warrior
[rubywarrior]: https://github.com/ryanb/ruby-warrior
[terminalizer]: https://terminalizer.com/
[crates.io]: https://crates.io
[docs.rs]: https://docs.rs
[cargo]: https://doc.rust-lang.org/book/ch01-03-hello-cargo.html
[queue]: https://docs.rs/releases/queue
[pr]: https://github.com/rust-lang/docs.rs/pull/393
[rustc]: https://rustconf.com/schedule/#is-this-magic-ferris-explores-rustc
[macros]: https://gitlab.com/rcousineau/hello-rust-macros
[tablist]: https://addons.mozilla.org/en-US/firefox/addon/tab-list-sidebar/
[darkreader]: https://addons.mozilla.org/en-US/firefox/addon/darkreader
[newtab]: https://addons.mozilla.org/en-US/firefox/addon/new-tab-context
[ublock]: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin
