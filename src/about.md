# Blog

I'm Russell Cousineau. This is a blog about my programming adventures.

This is built using [mdBook](https://github.com/rust-lang-nursery/mdBook).

Source for the site is on [Gitlab](https://gitlab.com/rcousineau/blog).
